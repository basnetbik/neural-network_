#include <iostream>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <string>

using namespace std;

string training_data_file = "train-images-idx3-ubyte", test_data_file = "t10k-images-idx3-ubyte";
string training_labels_file = "train-labels-idx1-ubyte", test_labels_file = "t10k-labels-idx1-ubyte";

const int NUMBER_OF_TRAINING_DATA = 60000, NUMBER_OF_TEST_DATA = 10000;
int training_data_considered = NUMBER_OF_TRAINING_DATA;

const int NUMBER_OF_ROWS = 28, NUMBER_OF_COLUMNS = 28;

const int DATA_DIMENSION = NUMBER_OF_ROWS*NUMBER_OF_COLUMNS, LABELS_DIMENSION = 10;
const int NEURONS_HIDDEN_LAYER = 33;

const int SIGMOID = 0;
const int RELU = 1;
const int HYPERBOLIC_TANGENT = 2;
const int ACTIVATION = RELU;

const int EPOCHS = 30;


// Superparameter eta
double e = 0.01;

double train_data[NUMBER_OF_TRAINING_DATA][DATA_DIMENSION], test_data[NUMBER_OF_TEST_DATA][DATA_DIMENSION];
int train_labels[NUMBER_OF_TRAINING_DATA], train_labels_vectorized[NUMBER_OF_TRAINING_DATA][LABELS_DIMENSION];
int test_labels[NUMBER_OF_TEST_DATA], test_labels_vectorized[NUMBER_OF_TEST_DATA][LABELS_DIMENSION];

double w0[NEURONS_HIDDEN_LAYER][DATA_DIMENSION];
double w00[NEURONS_HIDDEN_LAYER][DATA_DIMENSION];
double b0[NEURONS_HIDDEN_LAYER], b00[NEURONS_HIDDEN_LAYER];
double s0[NEURONS_HIDDEN_LAYER], y_0[NEURONS_HIDDEN_LAYER], delta_hidden[NEURONS_HIDDEN_LAYER];

double w1[LABELS_DIMENSION][NEURONS_HIDDEN_LAYER], w10[LABELS_DIMENSION][NEURONS_HIDDEN_LAYER];
double b1[LABELS_DIMENSION], b10[LABELS_DIMENSION];
double s1[LABELS_DIMENSION], y_1[LABELS_DIMENSION], delta_output[LABELS_DIMENSION];

ofstream track_accuracy("track_accuracy");


int reverse_int(int i) {
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i>>8) & 255;
    ch3 = (i>>16) & 255;
    ch4 = (i>>24) & 255;
    return ((int) ch1<<24) + ((int) ch2<<16) + ((int) ch3<<8) + ch4;
}

void read_MNIST_labels(string label_file, int vec[]) {
    ifstream file (label_file, ios::binary);

    if (file.is_open()) {
        int magic_number = 0, number_of_items = 0;

        file.read((char*) &magic_number, sizeof(magic_number));
        magic_number = reverse_int(magic_number);
        file.read((char*) &number_of_items, sizeof(number_of_items));
        number_of_items = reverse_int(number_of_items);

        for(int i = 0; i < number_of_items; i++) {
            unsigned char temp = 0;
            file.read((char*) &temp, sizeof(temp));
            vec[i]= (int)temp;
        }
    }
}

void read_MNIST_data(string data_file, double array[][DATA_DIMENSION]) {
    ifstream file(data_file, ios::binary);

    if (file.is_open()) {
        int magic_number, number_of_images, rows, columns;

        file.read((char*)&magic_number, sizeof(magic_number));
        magic_number = reverse_int(magic_number);

        file.read((char*)&number_of_images, sizeof(number_of_images));
        number_of_images = reverse_int(number_of_images);

        file.read((char*)&rows, sizeof(rows));
        rows = reverse_int(rows);
        assert (rows==NUMBER_OF_ROWS);

        file.read((char*)&columns, sizeof(columns));
        columns = reverse_int(columns);
        assert (columns==NUMBER_OF_COLUMNS);

        for(int i=0; i<number_of_images; i++)
            for(int r=0; r<NUMBER_OF_ROWS; r++)
                for(int c=0; c<NUMBER_OF_COLUMNS; c++) {
                    unsigned char temp = 0;
                    file.read((char*)&temp, sizeof(temp));
                    array[i][(NUMBER_OF_ROWS*r)+c] = (double) ((temp)*1.0)/255;
                }
    }
}

void vectorize_labels(int vectorized_labels[][LABELS_DIMENSION], int label[], int type=0) {
    int n = type==0 ? NUMBER_OF_TRAINING_DATA : NUMBER_OF_TEST_DATA;
    for (int i=0; i<n; i++)
        for (int j=0; j<LABELS_DIMENSION; j++)
            vectorized_labels[i][j] = j==label[i] ? 1 : 0;
}

void read_data() {
    read_MNIST_data(training_data_file, train_data);
    read_MNIST_labels(training_labels_file, train_labels);
    vectorize_labels(train_labels_vectorized, train_labels, 0);

    read_MNIST_data(test_data_file, test_data);
    read_MNIST_labels(test_labels_file, test_labels);
    vectorize_labels(test_labels_vectorized, test_labels, 1);
}

double gaussian_random() {
 //   return (double(rand()) / double(RAND_MAX))*2*0.12 - 0.12;

    double g_random = 0;
    for (int i=0; i<12; i++)
        g_random += double(rand()) / double(RAND_MAX);
    g_random = (g_random-6)/12;
    return g_random;
}

void init_weights() {
    int i, j;

    // w00, b00
    for (i=0; i<NEURONS_HIDDEN_LAYER; i++) {
        for (j=0; j<DATA_DIMENSION; j++)
            w00[i][j] = gaussian_random();

        b00[i] = gaussian_random();
    }

    // w10, b10
    for (i=0; i<LABELS_DIMENSION; i++) {
        for (j=0; j<NEURONS_HIDDEN_LAYER; j++)
            w10[i][j] = gaussian_random();

        b10[i] = gaussian_random();
    }
}


void update_weights() {
    int i, j, k, l;

    // w00, b00
    for (i=0; i<NEURONS_HIDDEN_LAYER; i++) {
        for (j=0; j<DATA_DIMENSION; j++)
            w00[i][j] = w0[i][j];

        b00[i] = b0[i];
    }

    // w10, b10
    for (i=0; i<LABELS_DIMENSION; i++) {
        for (j=0; j<NEURONS_HIDDEN_LAYER; j++)
            w10[i][j] = w1[i][j];

        b10[i] = b1[i];
    }
}

double activate(double x) {
    if (ACTIVATION==RELU )
        return x<0 ? 0.01*x : x;
    else if (ACTIVATION==SIGMOID)
        return 1/(1+exp(-1*x));
    else if (ACTIVATION==HYPERBOLIC_TANGENT)
        return tanh(x);
}

double activation_derivative(double x) {
    if (ACTIVATION==RELU)
        return x<0 ? 0.01: 1.0;
    else if (ACTIVATION==SIGMOID)
        return x * (1-x);
    else if (ACTIVATION==HYPERBOLIC_TANGENT)
        return (1-x*x);
}

void test_train();
void test_test();

void train(double data[][DATA_DIMENSION], int labels_[], int labels[][LABELS_DIMENSION]) {
    cout << "---- train start\n";

    // Declaration and initialization of parameters
    init_weights();

    int i, j, k;
    double temp;

    // Training of the neural network
	for (int r = 0; r <= EPOCHS; r++)
	{
        cout << "BEGINING OF EPOCH " << r<< endl;
		for (k = 0; k < training_data_considered; k++)
		{
            // FORWARD PROPAGATION
            // hidden layer
            for (i=0; i < NEURONS_HIDDEN_LAYER; i++) {
                temp = b00[i];

                for (j=0; j < DATA_DIMENSION; j++) {
                    temp += w00[i][j]*data[k][j];
                }

                s0[i] = temp;
                y_0[i] = activate(temp);
            }

            // output layer
            for (i=0; i < LABELS_DIMENSION; i++) {
                temp = b10[i];

                for (j=0; j < NEURONS_HIDDEN_LAYER; j++)
                    temp += w10[i][j] * y_0[j];

                s1[i] = temp;
                y_1[i] = activate(temp);
            }

            // BACK PROPAGATION
            // ERROR SIGNALS, GRADIENT COMPUTATION AND WEIGHT UPDATES
            // output layer
            for (i=0; i < LABELS_DIMENSION; i++) {
                // error signals
                delta_output[i] = (y_1[i] - labels[k][i]);

                // update w1 and b1
                for (j=0; j < NEURONS_HIDDEN_LAYER; j++) {
                    w1[i][j] = w10[i][j] - e * delta_output[i] * y_0[j];
                }

                b1[i] = b10[i] - e * delta_output[i] * 1;
            }

            // hidden layer
            for (i=0; i < NEURONS_HIDDEN_LAYER; i++) {
                // error signals
                temp = 0;

                for(j=0; j < LABELS_DIMENSION; j++)
                    temp += delta_output[j] * w10[j][i];

                delta_hidden[i] = temp*activation_derivative(y_0[i]);

                // update w0 and b0
                for (j=0; j < DATA_DIMENSION; j++)
                    w0[i][j] = w00[i][j] - e * delta_hidden[i] * data[k][j];

                b0[i] = b00[i] - e * delta_hidden[i] * 1;
            }

            // TRAIN FURTHER
            update_weights();
		}

		test_train();
		test_test();

		track_accuracy << "End of Epoch " << r << endl;
		cout << " End of Epoch " << r << endl;
	}
    cout << "epoch end\n";

    track_accuracy.close();

    cout << "---- train end\n";
 }

 void test_train() {
    cout << "\ntest on train data\n";

    double temp;
    double accuracy=0;
    int i, j, k;
    for (k = 0; k < training_data_considered; k++)
    {
       // FORWARD PROPAGATION
        // hidden layer
        for (i=0; i < NEURONS_HIDDEN_LAYER; i++) {
            temp = b00[i];

            for (j=0; j < DATA_DIMENSION; j++) {
                temp += w00[i][j]*train_data[k][j];
            }

            s0[i] = temp;
            y_0[i] = activate(temp);
        }

        // output layer
        for (i=0; i < LABELS_DIMENSION; i++) {
            temp = b10[i];

            for (j=0; j < NEURONS_HIDDEN_LAYER; j++)
                temp += w10[i][j] * y_0[j];

            s1[i] = temp;
            y_1[i] = activate(temp);
        }

        temp = max_element(y_1, y_1+LABELS_DIMENSION) - y_1;

        accuracy += train_labels[k] == temp ? 1 : 0;
    }

    accuracy = ((accuracy*1.0)/training_data_considered)*100;
    cout << "accuracy=" << accuracy << "%" << endl;
    track_accuracy << "train: " << accuracy << endl;
 }

 void test_test() {
    cout << "\ntest on test data\n";

    double temp;
    double accuracy=0;
    int i, j, k;
    for (k = 0; k < NUMBER_OF_TEST_DATA; k++)
    {
        // FORWARD PROPAGATION
        // hidden layer
        for (i=0; i < NEURONS_HIDDEN_LAYER; i++) {
            temp = b00[i];

            for (j=0; j < DATA_DIMENSION; j++) {
                temp += w00[i][j]*test_data[k][j];
            }

            s0[i] = temp;
            y_0[i] = activate(temp);
        }

        // output layer
        for (i=0; i < LABELS_DIMENSION; i++) {
            temp = b10[i];

            for (j=0; j < NEURONS_HIDDEN_LAYER; j++)
                temp += w10[i][j] * y_0[j];

            s1[i] = temp;
            y_1[i] = activate(temp);
        }

        temp = max_element(y_1, y_1+LABELS_DIMENSION) - y_1;

        accuracy += test_labels[k] == temp ? 1 : 0;
    }

    accuracy = ((accuracy*1.0)/NUMBER_OF_TEST_DATA)*100;
    cout << "accuracy=" << accuracy << "%" << endl;
    track_accuracy << "test: " << accuracy << endl;
 }

int main()
{
    read_data();

    train(train_data, train_labels, train_labels_vectorized);
    test_train();
    test_test();

    return 0;
}
