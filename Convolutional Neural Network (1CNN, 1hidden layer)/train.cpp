#include <iostream>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <string>

using namespace std;

string training_data_file = "train-images-idx3-ubyte", test_data_file = "t10k-images-idx3-ubyte";
string training_labels_file = "train-labels-idx1-ubyte", test_labels_file = "t10k-labels-idx1-ubyte";

const int NUMBER_OF_TRAINING_DATA = 60000, NUMBER_OF_TEST_DATA = 10000;
int training_data_considered = NUMBER_OF_TRAINING_DATA;

const int NUMBER_OF_ROWS = 28, NUMBER_OF_COLUMNS = 28;

const int DATA_DIMENSION = NUMBER_OF_ROWS*NUMBER_OF_COLUMNS, LABELS_DIMENSION = 10;
const int NEURONS_HIDDEN_LAYER = 45;

const int NUMBER_OF_FEATURE_MAPS = 8;
const int SLIDING_WINDOW_SIZE = 5;
const int FEATURE_MAP_SIZE = NUMBER_OF_ROWS - (SLIDING_WINDOW_SIZE-1);      // 24
const int MAX_WINDOW_SIZE = 2;
const int DOWNSIZED_FEATURE_MAP_SIZE = FEATURE_MAP_SIZE / MAX_WINDOW_SIZE;   // 12

const int SIGMOID = 0;
const int RELU = 1;
const int ACTIVATION = RELU;

const int EPOCHS = 30;


// Superparameter eta
double e = 0.01;

double train_data[NUMBER_OF_TRAINING_DATA][DATA_DIMENSION], test_data[NUMBER_OF_TEST_DATA][DATA_DIMENSION];
int train_labels[NUMBER_OF_TRAINING_DATA], train_labels_vectorized[NUMBER_OF_TRAINING_DATA][LABELS_DIMENSION];
int test_labels[NUMBER_OF_TEST_DATA], test_labels_vectorized[NUMBER_OF_TEST_DATA][LABELS_DIMENSION];

double w0[NUMBER_OF_FEATURE_MAPS][SLIDING_WINDOW_SIZE][SLIDING_WINDOW_SIZE], w00[NUMBER_OF_FEATURE_MAPS][SLIDING_WINDOW_SIZE][SLIDING_WINDOW_SIZE];
double b0[NUMBER_OF_FEATURE_MAPS], b00[NUMBER_OF_FEATURE_MAPS];
double s0[NUMBER_OF_FEATURE_MAPS][FEATURE_MAP_SIZE][FEATURE_MAP_SIZE], y_0[NUMBER_OF_FEATURE_MAPS][FEATURE_MAP_SIZE][FEATURE_MAP_SIZE];

double s1[NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE];
double delta_feature_map[NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE];
int max_index[NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE][2], max_index_r, max_index_c;

double w2[NEURONS_HIDDEN_LAYER][NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE];
double w20[NEURONS_HIDDEN_LAYER][NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE];
double b2[NEURONS_HIDDEN_LAYER], b20[NEURONS_HIDDEN_LAYER];
double s2[NEURONS_HIDDEN_LAYER], y2[NEURONS_HIDDEN_LAYER], delta_hidden[NEURONS_HIDDEN_LAYER];

double w3[LABELS_DIMENSION][NEURONS_HIDDEN_LAYER], w30[LABELS_DIMENSION][NEURONS_HIDDEN_LAYER];
double b3[LABELS_DIMENSION], b30[LABELS_DIMENSION];
double s3[LABELS_DIMENSION], y3[LABELS_DIMENSION], delta_output[LABELS_DIMENSION];

ofstream track_accuracy("track_accuracy");


int reverse_int(int i) {
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i>>8) & 255;
    ch3 = (i>>16) & 255;
    ch4 = (i>>24) & 255;
    return ((int) ch1<<24) + ((int) ch2<<16) + ((int) ch3<<8) + ch4;
}

void save_trained_weights(double w00[][SLIDING_WINDOW_SIZE][SLIDING_WINDOW_SIZE], double b00[], double w20[][NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE], double b20[], double w30[][NEURONS_HIDDEN_LAYER], double b30[]) {
    ofstream trained_weights("trained_weights.h");
    ofstream trained_weights_ui("TrainedWeights.cs");

    trained_weights << "#ifndef TRAINED_WEIGHTS_H\n#define TRAINED_WEIGHTS_H\n\n";
    trained_weights << "namespace trained_weights {\n\n";
    trained_weights << "const int SLIDING_WINDOW_SIZE = 5;\nconst int NUMBER_OF_FEATURE_MAPS = 6;\nconst int DOWNSIZED_FEATURE_MAP_SIZE = 12;\nconst int NEURONS_HIDDEN_LAYER = 33;\n\n";
    trained_weights << "void set_weights(double w0[][SLIDING_WINDOW_SIZE][SLIDING_WINDOW_SIZE], double b0[], double w2[][NUMBER_OF_FEATURE_MAPS][DOWNSIZED_FEATURE_MAP_SIZE][DOWNSIZED_FEATURE_MAP_SIZE], double b2[], double w3[][NEURONS_HIDDEN_LAYER], double b3[]) {\n";

    trained_weights_ui << "using System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Text;\nusing System.Threading.Tasks;\n\n";
    trained_weights_ui << "namespace TestBackpropagation\n{\n\tclass TrainedWeights\n\t{\n\t\tpublic TrainedWeights(double[,,] w0, double[] b0, double[,,,] w2, double[] b2, double[,] w3, double[] b3)\n\t\t{\n";

    for (int l=0; l<NUMBER_OF_FEATURE_MAPS; l++)
        for (int i=0; i<SLIDING_WINDOW_SIZE; i++)
            for (int j=0; j<SLIDING_WINDOW_SIZE; j++) {
                trained_weights << "\t\t\tw0[" << l << "][" << i << "][" << j << "] = " << w00[l][i][j] << ";\n";
                trained_weights_ui << "\t\t\tw0[" << l << "," << i << "," << j << "] = " << w00[l][i][j] << ";\n";
            }

    for (int i=0; i<NUMBER_OF_FEATURE_MAPS; i++) {
        trained_weights << "\t\t\tb0[" << i << "] = " << b00[i] << ";\n";
        trained_weights_ui << "\t\t\tb0[" << i << "] = " << b00[i] << ";\n";
    }

    for (int i=0; i<NEURONS_HIDDEN_LAYER; i++)
        for (int j=0; j<NUMBER_OF_FEATURE_MAPS; j++)
            for (int k=0; k<DOWNSIZED_FEATURE_MAP_SIZE; k++)
                for (int l=0; l<DOWNSIZED_FEATURE_MAP_SIZE; l++) {
                    trained_weights << "\t\t\tw2[" << i << "][" << j << "][" << k << "][" << l << "] = " << w20[i][j][k][l] << ";\n";
                    trained_weights_ui << "\t\t\tw2[" << i << "," << j << "," << k << "," << l << "] = " << w20[i][j][k][l] << ";\n";
                }


    for (int i=0; i<NEURONS_HIDDEN_LAYER; i++) {
        trained_weights << "\t\t\tb2[" << i << "] = " << b20[i] << ";\n";
        trained_weights_ui << "\t\t\tb2[" << i << "] = " << b20[i] << ";\n";
    }

    for (int i=0; i<LABELS_DIMENSION; i++)
        for (int j=0; j<NEURONS_HIDDEN_LAYER; j++) {
            trained_weights << "\t\t\tw3[" << i << "][" << j << "] = " << w30[i][j] << ";\n";
            trained_weights_ui << "\t\t\tw3[" << i << "," << j << "] = " << w30[i][j] << ";\n";
        }

    for (int i=0; i<LABELS_DIMENSION; i++) {
        trained_weights << "\t\t\tb3[" << i << "] = " << b30[i] << ";\n";
        trained_weights_ui << "\t\t\tb3[" << i << "] = " << b30[i] << ";\n";
    }

    trained_weights << "}\n\n";
    trained_weights << "}\n\n#endif\n";
    trained_weights.close();

    trained_weights_ui << "\n\t\t}\n\t}\n}";
    trained_weights_ui.close();
}

void read_MNIST_labels(string label_file, int vec[]) {
    ifstream file (label_file, ios::binary);

    if (file.is_open()) {
        int magic_number = 0, number_of_items = 0;

        file.read((char*) &magic_number, sizeof(magic_number));
        magic_number = reverse_int(magic_number);
        file.read((char*) &number_of_items, sizeof(number_of_items));
        number_of_items = reverse_int(number_of_items);

        for(int i = 0; i < number_of_items; i++) {
            unsigned char temp = 0;
            file.read((char*) &temp, sizeof(temp));
            vec[i]= (int)temp;
        }
    }
}

void read_MNIST_data(string data_file, double array[][DATA_DIMENSION]) {
    ifstream file(data_file, ios::binary);

    if (file.is_open()) {
        int magic_number, number_of_images, rows, columns;

        file.read((char*)&magic_number, sizeof(magic_number));
        magic_number = reverse_int(magic_number);

        file.read((char*)&number_of_images, sizeof(number_of_images));
        number_of_images = reverse_int(number_of_images);

        file.read((char*)&rows, sizeof(rows));
        rows = reverse_int(rows);
        assert (rows==NUMBER_OF_ROWS);

        file.read((char*)&columns, sizeof(columns));
        columns = reverse_int(columns);
        assert (columns==NUMBER_OF_COLUMNS);

        for(int i=0; i<number_of_images; i++)
            for(int r=0; r<NUMBER_OF_ROWS; r++)
                for(int c=0; c<NUMBER_OF_COLUMNS; c++) {
                    unsigned char temp = 0;
                    file.read((char*)&temp, sizeof(temp));
                    array[i][(NUMBER_OF_ROWS*r)+c] = (double) ((temp)*1.0)/255;
                }
    }
}

void vectorize_labels(int vectorized_labels[][LABELS_DIMENSION], int label[], int type=0) {
    int n = type==0 ? NUMBER_OF_TRAINING_DATA : NUMBER_OF_TEST_DATA;
    for (int i=0; i<n; i++)
        for (int j=0; j<LABELS_DIMENSION; j++)
            vectorized_labels[i][j] = j==label[i] ? 1 : 0;
}

void read_data() {
    read_MNIST_data(training_data_file, train_data);
    read_MNIST_labels(training_labels_file, train_labels);
    vectorize_labels(train_labels_vectorized, train_labels, 0);

    read_MNIST_data(test_data_file, test_data);
    read_MNIST_labels(test_labels_file, test_labels);
    vectorize_labels(test_labels_vectorized, test_labels, 1);
}

double gaussian_random() {
    return (double(rand()) / double(RAND_MAX))*2*0.12 - 0.12;

    double g_random = 0;
    for (int i=0; i<12; i++)
        g_random += double(rand()) / double(RAND_MAX);
    g_random = (g_random-6)/12;
    return g_random;
}

void init_weights() {
    int i, j, k, l;

    // w00
    for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++)
        for (j=0; j<SLIDING_WINDOW_SIZE; j++)
            for (k=0; k<SLIDING_WINDOW_SIZE; k++)
                w00[l][j][k] = gaussian_random();

    // b00, w20
    for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++) {
        for (k=0; k<12; k++)
            for (l=0; l<12; l++)
                for (j=0; j<NEURONS_HIDDEN_LAYER; j++)
                    w20[j][i][k][l] = gaussian_random();

        b00[i] = gaussian_random();
    }

    // b20, w30
    for (i=0; i<NEURONS_HIDDEN_LAYER; i++) {
        for (j=0; j<LABELS_DIMENSION; j++)
            w30[j][i] = gaussian_random();

        b20[i] = gaussian_random();
    }

    // b30
    for (i=0; i<LABELS_DIMENSION; i++)
        b30[i] = gaussian_random();
}


void update_weights() {
    int i, j, k, l;

    // w00
    for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++)
        for (j=0; j<SLIDING_WINDOW_SIZE; j++)
            for (k=0; k<SLIDING_WINDOW_SIZE; k++)
                w00[l][j][k] = w0[l][j][k];

    // b00, w20
    for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++) {
        for (k=0; k<12; k++)
            for (l=0; l<12; l++)
                for (j=0; j<NEURONS_HIDDEN_LAYER; j++)
                    w20[j][i][k][l] = w2[j][i][k][l];

        b00[i] = b0[i];
    }

    // b20, w30
    for (i=0; i<NEURONS_HIDDEN_LAYER; i++) {
        for (j=0; j<LABELS_DIMENSION; j++)
            w30[j][i] = w3[j][i];

        b20[i] = b2[i];
    }

    // b30
    for (i=0; i<LABELS_DIMENSION; i++)
        b30[i] =  b3[i];
}

double activate(double x) {
    if (ACTIVATION==RELU )
        return x<0 ? 0.01*x : x;
    else if (ACTIVATION==SIGMOID)
        return 1/(1+exp(-1*x));
}

double activation_derivative(double x) {
    if (ACTIVATION==RELU)
        return x<0 ? 0.01: 1.0;
    else if (ACTIVATION==SIGMOID)
        return x * (1-x);
}

void test_train();
void test_test();

void train(double data[][DATA_DIMENSION], int labels_[], int labels[][LABELS_DIMENSION]) {
    cout << "---- train start\n";

    // Declaration and initialization of parameters
    init_weights();

    int i, j, k, l, m, n, o, p;
    double temp;

    // Training of the neural network
	for (int r = 0; r <= EPOCHS; r++)
	{
        cout << "BEGINING OF EPOCH " << r<< endl;
		for (k = 0; k < training_data_considered; k++)
		{
            // FORWARD PROPAGATION
            for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++) {
                // generate feature map
                for (m=0; m<FEATURE_MAP_SIZE; m++) {
                    for (n=0; n<FEATURE_MAP_SIZE; n++) {
                        temp = b00[l];

                        for (i=0; i<SLIDING_WINDOW_SIZE; i++)
                            for (j=0; j<SLIDING_WINDOW_SIZE; j++)
                                temp += w00[l][i][j]*data[k][(i+m)*NUMBER_OF_COLUMNS + (j+n)];

                        s0[l][m][n] = temp;
                        y_0[l][m][n] = activate(temp);
                    }
                }

                // downsize feature map
                for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++) {
                    for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++) {
                        temp = y_0[l][MAX_WINDOW_SIZE*m][MAX_WINDOW_SIZE*n];
                        max_index[l][m][n][0] = MAX_WINDOW_SIZE*m;
                        max_index[l][m][n][1] = MAX_WINDOW_SIZE*n;

                        for (i=m*MAX_WINDOW_SIZE; i<m*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; i++)
                            for (j=n*MAX_WINDOW_SIZE; j<n*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; j++)
                                if (y_0[l][i][j] > temp) {
                                    temp = y_0[l][i][j];
                                    max_index[l][m][n][0] = i;
                                    max_index[l][m][n][1] = j;
                                }

                        s1[l][m][n] = temp;
                    }
                }
            }

            // hidden layer
            for (l=0; l<NEURONS_HIDDEN_LAYER; l++) {
                temp = b20[l];

                for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++)
                    for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++)
                        for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++)
                            temp+= w20[l][i][m][n] * s1[i][m][n];

                s2[l] = temp;
                y2[l] = activate(temp);
            }

            // output layer
            for (l=0; l<LABELS_DIMENSION; l++) {
                temp = b30[l];

                for (i=0; i<NEURONS_HIDDEN_LAYER; i++)
                    temp += w30[l][i] * y2[i];

                s3[l] = temp;
                y3[l] = activate(temp);
            }

            // BACK PROPAGATION
            // ERROR SIGNALS, GRADIENT COMPUTATION AND WEIGHT UPDATES
            // output layer
            for (l=0; l<LABELS_DIMENSION; l++) {
                // error signals
                delta_output[l] = (y3[l] - labels[k][l]) * activation_derivative(y3[l]);

                // update w3 and b3
                for (n=0; n<NEURONS_HIDDEN_LAYER; n++) {
                    w3[l][n] = w30[l][n] - e * delta_output[l] * y2[n];
                }

                b3[l] = b30[l] - e * delta_output[l] * 1;
            }

            // hidden layer
            for (l=0; l<NEURONS_HIDDEN_LAYER; l++) {
                // error signals
                temp = 0;

                for(i=0; i<LABELS_DIMENSION; i++)
                    temp += delta_output[i] * w30[i][l];

                delta_hidden[l] = temp*activation_derivative(y2[l]);

                // update w2 and b2
                for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++)
                    for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++)
                        for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++)
                            w2[l][i][m][n] = w20[l][i][m][n] - e * delta_hidden[l] * s1[i][m][n];

                b2[l] = b20[l] - e * delta_hidden[l] * 1;
            }


            // feature maps
            // error signals, update w0 and b0
            for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++) {
                for (m=0; m<SLIDING_WINDOW_SIZE; m++)
                    for (n=0; n<SLIDING_WINDOW_SIZE; n++)
                        w0[l][m][n] = 0;

                b0[l] = 0;

                for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++) {
                    for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++) {
                        temp = 0;

                        for (i=0; i<NEURONS_HIDDEN_LAYER; i++)
                            temp += delta_hidden[i] * w20[i][l][m][n];

                        max_index_r = max_index[l][m][n][0];
                        max_index_c = max_index[l][m][n][1];

                        delta_feature_map[l][m][n] = temp * activation_derivative(y_0[l][max_index_r][max_index_c]);

                        for (i=0; i<SLIDING_WINDOW_SIZE; i++)
                            for (j=0; j<SLIDING_WINDOW_SIZE; j++)
                                w0[l][i][j] += delta_feature_map[l][m][n]* data[k][(max_index_r+i)*NUMBER_OF_COLUMNS + (max_index_c+j)];

                        b0[l] += delta_feature_map[l][m][n] * 1;
                    }
                }

                for (m=0; m<SLIDING_WINDOW_SIZE; m++)
                    for (n=0; n<SLIDING_WINDOW_SIZE; n++)
                        w0[l][m][n] = w00[l][m][n] - e*w0[l][m][n];

                b0[l] = b00[l] - e * b0[l];
            }

            // TRAIN FURTHER
            update_weights();
		}

		test_train();
		test_test();

		track_accuracy << "End of Epoch " << r << endl;
		cout << " End of Epoch " << r << endl;
	}
    cout << "epoch end\n";

    track_accuracy.close();

    save_trained_weights(w00, b00, w20, b20, w30, b30);

    cout << "---- train end\n";
 }

 void test_train() {
    cout << "\ntest on train data\n";

    double temp;
    double accuracy=0;
    int i, j, k, l, m, n;
    for (k = 0; k < training_data_considered; k++)
    {
       // FORWARD PROPAGATION
        for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++) {
            // generate feature map
            for (m=0; m<FEATURE_MAP_SIZE; m++) {
                for (n=0; n<FEATURE_MAP_SIZE; n++) {
                    temp = b00[l];

                    for (i=0; i<SLIDING_WINDOW_SIZE; i++)
                        for (j=0; j<SLIDING_WINDOW_SIZE; j++)
                            temp += w00[l][i][j]*train_data[k][(i+m)*NUMBER_OF_COLUMNS + (j+n)];

                    s0[l][m][n] = temp;
                    y_0[l][m][n] = activate(temp);
                }
            }

            // downsize feature map
            for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++) {
                for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++) {
                    temp = y_0[l][MAX_WINDOW_SIZE*m][MAX_WINDOW_SIZE*n];

                    for (i=m*MAX_WINDOW_SIZE; i<m*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; i++)
                        for (j=n*MAX_WINDOW_SIZE; j<n*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; j++)
                            if (y_0[l][i][j] > temp) {
                                temp = y_0[l][i][j];
                            }

                    s1[l][m][n] = temp;
                }
            }
        }

        // hidden layer
        for (l=0; l<NEURONS_HIDDEN_LAYER; l++) {
            temp = b20[l];

            for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++)
                for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++)
                    for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++)
                        temp+= w20[l][i][m][n] * s1[i][m][n];

            s2[l] = temp;
            y2[l] = activate(temp);
        }

        // output layer
        for (l=0; l<LABELS_DIMENSION; l++) {
            temp = b30[l];

            for (i=0; i<NEURONS_HIDDEN_LAYER; i++)
                temp += w30[l][i] * y2[i];

            s3[l] = temp;
            y3[l] = activate(temp);
        }
        temp = max_element(y3, y3+LABELS_DIMENSION) - y3;

        accuracy += train_labels[k] == temp ? 1 : 0;
    }

    accuracy = ((accuracy*1.0)/training_data_considered)*100;
    cout << "accuracy=" << accuracy << "%" << endl;
    track_accuracy << "train: " << accuracy << endl;
 }

 void test_test() {
    cout << "\ntest on test data\n";

    double temp;
    double accuracy=0;
    int i, j, k, l, m, n;
    for (k = 0; k < NUMBER_OF_TEST_DATA; k++)
    {
        // FORWARD PROPAGATION
        for (l=0; l<NUMBER_OF_FEATURE_MAPS; l++) {
            // generate feature map
            for (m=0; m<FEATURE_MAP_SIZE; m++) {
                for (n=0; n<FEATURE_MAP_SIZE; n++) {
                    temp = b00[l];

                    for (i=0; i<SLIDING_WINDOW_SIZE; i++)
                        for (j=0; j<SLIDING_WINDOW_SIZE; j++)
                            temp += w00[l][i][j]*test_data[k][(i+m)*NUMBER_OF_COLUMNS + (j+n)];

                    s0[l][m][n] = temp;
                    y_0[l][m][n] = activate(temp);
                }
            }

            // downsize feature map
            for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++) {
                for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++) {
                    temp = y_0[l][MAX_WINDOW_SIZE*m][MAX_WINDOW_SIZE*n];

                    for (i=m*MAX_WINDOW_SIZE; i<m*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; i++)
                        for (j=n*MAX_WINDOW_SIZE; j<n*MAX_WINDOW_SIZE+MAX_WINDOW_SIZE; j++)
                            if (y_0[l][i][j] > temp) {
                                temp = y_0[l][i][j];
                            }

                    s1[l][m][n] = temp;
                }
            }
        }

        // hidden layer
        for (l=0; l<NEURONS_HIDDEN_LAYER; l++) {
            temp = b20[l];

            for (i=0; i<NUMBER_OF_FEATURE_MAPS; i++)
                for (m=0; m<DOWNSIZED_FEATURE_MAP_SIZE; m++)
                    for (n=0; n<DOWNSIZED_FEATURE_MAP_SIZE; n++)
                        temp+= w20[l][i][m][n] * s1[i][m][n];

            s2[l] = temp;
            y2[l] = activate(temp);
        }

        // output layer
        for (l=0; l<LABELS_DIMENSION; l++) {
            temp = b30[l];

            for (i=0; i<NEURONS_HIDDEN_LAYER; i++)
                temp += w30[l][i] * y2[i];

            s3[l] = temp;
            y3[l] = activate(temp);
        }

        temp = max_element(y3, y3+LABELS_DIMENSION) - y3;

        accuracy += test_labels[k] == temp ? 1 : 0;
    }

    accuracy = ((accuracy*1.0)/NUMBER_OF_TEST_DATA)*100;
    cout << "accuracy=" << accuracy << "%" << endl;
    track_accuracy << "test: " << accuracy << endl;
 }

int main()
{
    read_data();

    train(train_data, train_labels, train_labels_vectorized);
    test_train();
    test_test();

    return 0;
}
